
<?php




class Czlowiek
{

    public function bezpiecznie($kolor_sygnalizatora)
    {
        return $kolor_sygnalizatora == 'zielony';
    }


    public function isc($sygnalizator)
    {
        if ($this->bezpiecznie($sygnalizator->kolor_sygnalizatora)) {
            echo " uznaje droge za bezpieczna i przechodzi przez pasy </br>";
        } else {
            echo " uznaje droge za niebezpiecznie i NIE przechodzi przez pasy </br>";
        }
    }
}




class Sygnalizator
{
    const RED_LIGHT = 'czerwony';

    public $kolor_sygnalizatora = '';
    public $change_after = 0;

    public function __construct($change_after, $kolor_sygnalizatora)
    {
        $this->change_after = $change_after;
        $this->kolor_sygnalizatora = $kolor_sygnalizatora;
    }



    public function zmien_kolor($counter)
    {
        if ($counter % $this->change_after === 0) {
            $this->zmiana();    
            echo ' zmiana';
        }

        return  $this->kolor_sygnalizatora;

        
    }

    private function zmiana() {
        $this->kolor_sygnalizatora = $this->kolor_sygnalizatora === 'zielony' ? self::RED_LIGHT : 'zielony';
    }
}












?>
